@extends('admin.layout.index')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> admin</a></li>
        <li class="active">Thêm</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
              <div class="">
            <div class="box-header with-border">
              <h3 class="box-title">Thêm
             
              </h3>
            </div>
             @if(isset($message))
              <div class="alert alert-success">
              {{$message}}
              </div>
              @endif
            @if(session('Error'))
              <div class="alert alert-danger">
                {{session('Error')}}
              </div>
              @endif
            <!-- /.box-header -->
            <!-- form start -->
            
            <form role="form" action="{{route('addUrl')}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
                <div class="form-group">
                  <label for="title">Tiêu để</label>
                  <input required type="text" name="name" class="form-control" id="name" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Url</label>
                  <input required type="text" name="url" class="form-control" id="url" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="summary">Mô tả</label>
                  <textarea name="summary" class="form-control" id="" placeholder="Mô tả"></textarea>
                </div>
                
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Thêm</button>
              </div>
            </form>
        </div>
          </div>
          <!-- /.box -->

          


        </div>
        <!--/.col (left) -->
        
       
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection
