@extends('admin.layout.index')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrator
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Cập nhật</li>
      </ol>
    </section>
    @if(isset($data))
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
              <div class="">
            <div class="box-header with-border">
              <h3 class="box-title">Cập nhật
              @if(isset($message))
              {{$message}}
              @endif
              </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <form role="form" action="{{route('editContact')}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{$data->id}}">
              <div class="box-body">                
                
                <div class="form-group">
                  <label for="title">Công ty</label>
                  <input required type="text" name="name" value="{{$data->name}}" class="form-control" id="name" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Url</label>
                  <input required type="text" name="url" value="{{$data->url}}" class="form-control" id="url" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Slogan</label>
                  <input required type="text" name="slogan" value="{{$data->slogan}}" class="form-control" id="slogan" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Điện thoại</label>
                  <input required type="text" name="phone" value="{{$data->phone}}" class="form-control" id="phone" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Số fax</label>
                  <input required type="text" name="fax" value="{{$data->fax}}" class="form-control" id="fax" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Địa chỉ</label>
                  <input required type="text" name="address" value="{{$data->address}}" class="form-control" id="address" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">email</label>
                  <input required type="text" name="email" value="{{$data->email}}" class="form-control" id="email" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="title">Ghi chú</label>
                  <input required type="text" name="note" value="{{$data->note}}" class="form-control" id="note" placeholder="Tiêu đề">
                </div>
                <div class="form-group">
                  <label for="summary">Mô tả</label>
                  <textarea name="summary" class="form-control" id="" placeholder="Mô tả">{{$data->summary}}</textarea>
                </div>
                <div class="form-group">
                  @if($data->image !="")
                  <img src="{{$data->image}}" style="width:100px; max-height:200px"/>
                  @endif
                  <br>
                  <label for="exampleInputFile">Cập nhật hình</label>
                  <input type="file" id="image" name="image">
                  <p class="help-block">chỉ dùng [.png .jpg .gif]</p>
                </div>

               
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Cập nhật</button>
              </div>
            </form>
        </div>
          </div>
          <!-- /.box -->

          


        </div>
        <!--/.col (left) -->
        
       
      </div>
      <!-- /.row -->
    </section>
    @endif
  </div>
@endsection
