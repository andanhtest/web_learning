<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('code')->nullable();
            $table->longText('summary')->nullable();
            $table->integer('user_id')->default(0);
            $table->string('fullname_buyer')->nullable();
            $table->string('phone_buyer')->nullable();
            $table->string('email_buyer')->nullable();
            $table->string('address_buyer')->nullable();
            $table->string('fullname_receiver')->nullable();
            $table->string('phone_receiver')->nullable();
            $table->string('email_receiver')->nullable();
            $table->string('address_receiver')->nullable();
            $table->longText('location')->nullable();
            $table->longText('note')->nullable();
            $table->double('total')->default(0);
            $table->double('vat')->default(0);
            $table->integer('status')->default(1);
            $table->integer('type')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
