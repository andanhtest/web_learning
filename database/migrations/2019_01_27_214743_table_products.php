<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('alias')->nullable();
            $table->longText('summary')->nullable();
            $table->longText('content')->nullable();
            $table->string('images')->nullable();            
            $table->integer('price')->default(0);
            $table->integer('price_old')->default(0);
            $table->integer('hot')->default(0);
            $table->integer('new')->default(0);
            $table->integer('view')->default(0);
            $table->integer('special')->default(0);
            $table->integer('category_id')->default(0);
            $table->integer('status')->default(1);
            $table->integer('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
