<?php
/**
 * Created by PhpStorm.
 * User: lecongthang
 * Date: 29/11/2018
 * Time: 15:03
 */

namespace App\Service;


use Illuminate\Support\Facades\Storage;

class UploadService
{


    /**
     * Upload service, using when need upload file.
     * @param $file
     * @param $physic_path
     * @return string |null
     */
    public static function handleUploadFile($file, $physic_path)
    {
        $path = '';
        if (!is_null($file)) {
            $file_name = md5(time()) . '.' . $file->getClientOriginalExtension();
            $path = Storage::disk('public')->putFileAs($physic_path, $file, $file_name);
        }
        return $path;
    }

    public static function handleRemoveFile($file_link)
    {
        if (file_exists(storage_path('app/public/' . $file_link))) {
            Storage::disk('public')->delete($file_link);
        }
    }
}
