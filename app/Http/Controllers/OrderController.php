<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;
use App\OrderDetail;
use App\Helpers\Helper;

class OrderController extends Controller
{
    public function list(){
        $lists = Orders::orderBy('id','desc')->paginate(15);
        return view("admin.order.list",['lists'=>$lists]);
    }
    public function listStatus(Request $res){
        $lists = array();
        if(isset($res->status) && in_array($res->status,array(0,1,-1))){
            $lists = Orders::where("status",$res->status)
                            ->orderBy('id','desc')->paginate(15);
        }        
        return view("admin.order.list",['lists'=>$lists]);
    }
    public function edit(Request $res){
        $data = Orders::where("id","=",$res->id)->first();
        $order_detail = OrderDetail::where("order_id","=",$res->id)->get();
        return view("admin.order.edit",['data'=>$data,'order_detail'=>$order_detail]);
    }

    public function editOrderAction(Request $res){ 
        $ck = Orders::where("id","=",$res->id)->first();       
        $cate = Orders::where("id","=",$res->id);
        $data = array();
        $arr_status = array(1,-1);
        if($ck->status==0){            
            if(in_array($res->status,$arr_status)){
                $data['status'] = $res->status;    
                $cate->update($data);
            }            
        }
        if(isset($ck->status_delivery) && (in_array($res->status,$arr_status) or $ck->status!=0)){
            $arr_delivery = array(1,-1);
            if(in_array($res->status_delivery,$arr_delivery)){
                $data['status_delivery'] = $res->status_delivery;    
                $cate->update($data);
            }            
        }
        return redirect("admin/order/list");
    }
    public function deleteOrderAction(Request $res){        
        Orders::where("id","=",$res->id)->delete();
        return redirect("admin/order/list");
    }
}
