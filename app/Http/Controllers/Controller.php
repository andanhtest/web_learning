<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Auth;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    private $user;

    function __construct(Request $request){        
        $sv =  $_SERVER['REQUEST_URI'];
        $str = str_replace("/","_",$sv);
        if(count($str)>0){
            $arr = explode("admin_",$str);
            if(count($arr)>1){
                $str2 = explode('_',$arr[1]);
                view()->share("menu_active",$str2[0]);
            }else{
                view()->share("menu_active",""); 
            }
        }else{
            view()->share("menu_active",""); 
        }             
    }
}
