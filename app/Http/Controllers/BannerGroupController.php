<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BannerGroup;
use App\Helpers\Helper;

class BannerGroupController extends Controller
{
    public function list(){
        $lists = BannerGroup::orderBy('id','desc')->get();
        return view("admin.banner-group.list",['lists'=>$lists]);
    }
    public function add(){
        return view('admin.banner-group.add');
    }
    public function edit(Request $res){
        $data = BannerGroup::where("id","=",$res->id)->first();
        return view("admin.banner-group.edit",['data'=>$data]);
    }

    public function editBannerGroupAction(Request $res){        
        $cate = BannerGroup::where("id","=",$res->id);
        $data = array();
        $data['name'] = $res->title;
        $data['status'] = $res->status==1?1:0;
        $cate->update($data);
        return redirect("admin/banner-group/list");
    }
    //action
    public function addBannerGroupAction(Request $res){    
        $data = new BannerGroup();
        $data->name = $res->title;
        $data->alias = Helper::convert_alias($res->title);
        $data->status = $res->status==1?1:0;
        $data->save();
        return redirect("admin/banner-group/list");
    }    
    public function deleteBannerGroupAction(Request $res){        
        BannerGroup::where("id","=",$res->id)->delete();
        return redirect("admin/banner-group/list");
    }
}
