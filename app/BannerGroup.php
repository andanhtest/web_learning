<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerGroup extends Model
{
    protected $table = "banner_group";
    public function banner(){
        return $this->hasMany("App\Banners",'type','id');
    }
}
